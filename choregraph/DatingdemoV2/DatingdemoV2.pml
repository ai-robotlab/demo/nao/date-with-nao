<?xml version="1.0" encoding="UTF-8" ?>
<Package name="DatingdemoV2" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="Datingdemo" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="contentement3" src="Datingdemo/Sounds/contentement3.wav" />
        <File name="court-boom" src="Datingdemo/Sounds/court-boom.wav" />
        <File name="fly" src="Datingdemo/Sounds/fly.wav" />
        <File name="gong" src="Datingdemo/Sounds/gong.mp3" />
        <File name="martinebondheartbeat" src="Datingdemo/Sounds/martinebondheartbeat.wav" />
        <File name="watch" src="Datingdemo/Sounds/watch.wav" />
        <File name="yawn3" src="Datingdemo/Sounds/yawn3.wav" />
    </Resources>
    <Topics />
    <IgnoredPaths />
</Package>
